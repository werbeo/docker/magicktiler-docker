FROM ubuntu:18.04

MAINTAINER dve@vergien.net

RUN apt-get -y update && \
	 apt-get -y install graphicsmagick default-jre

COPY bin/magicktiler.jar magicktiler.jar

ENTRYPOINT ["java", "-jar", "magicktiler.jar"]
