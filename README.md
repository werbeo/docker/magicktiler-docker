# MagickTiler Docker Image

A docker image for [MagickTiler](https://github.com/magicktiler/magicktiler).

MagickTiler is a tool to create tiled images, which allow smooth zoom and panning in a web browser.

## Usage

```docker run infinitenature/magicktiler``` will show the available options.

```docker run -v /host/in:/in -v /host/out/:/out infinitenature/magicktiler -i in -s zoomify -o /out``` will:

  1. mount the host folder ```/host/in``` to the container folder ```/in```
  1. mount the host folder ```/host/out``` to the container folder ```/out```
  1. tell MagikTiler to process the images from the container folder ```-i in``` 
  1. create zoomify style tiles ```-s zoomify```
  1. place the tiles int the container folder ```-o /out```

## License

This project is licensed under the Apache License 2.0

### MagickTiler

MagickTiler is licensed under the EUPL, Version 1.1 (http://ec.europa.eu/idabc/eupl).

IMPORTANT: The 'zoomify' folder contains a Flash (*.swf) file that is not part of the
MagickTiler software. This file is copyright by Zoomify Inc. and covered by its own 
licensing terms. Please refer to the accompanying Zoomify License Agreement
'license_Zoomify.txt' for details.
